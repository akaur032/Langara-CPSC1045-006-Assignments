var coloumns = 8; // number of coloumns in game
var rows = 8; // number of rows in game
let object = { x:coloumns/2, y: 0 }; // starting point of every new object
let points = 0;
let pointsArea = document.querySelector("#points");
let grid = createGrid(rows,coloumns); // creating and saving a grid

function createGrid(width, height) {
    var s = [];
    for (let row = 0; row < height; row ++) {
        let coloumn =[];
        for (let col = 0; col < width; col ++) {
           coloumn.push("E"); // E for empty grid cells
        }
        s.push(coloumn);
    };
    return s;
}

function drawGrid(){
    let gridArea = document.querySelector("#grid");
    let s = "";
    for (let i = 0; i < coloumns; i++) {
        for (let j = 0; j < rows; j++) {
            s += "<rect x='" + j * 30 + "' y='" + i * 30 +"' width ='30' height='30' stroke='black' fill='white'/>";
        }
    }
    gridArea.innerHTML = s;
}
window.drawGrid = drawGrid;

let objectArea = document.querySelector("#object");

function drawObject() {
    let s="";
    for (let i = 0; i < coloumns; i++) {
        for (let j = 0; j < rows; j++) {
            if(grid[i][j]=="S"){
               s+= "<rect x='" + j*30 + "' y='" + i*30 + "' height='30' width='30' stroke='black' fill=' blue '/>";
            }
        }
    }
    objectArea.innerHTML = s;
}

function refresh() {
        if(object.y < coloumns-1){
            if (object.y<rows-1 && grid[object.y + 1][object.x] != "S") {
                object.y += 1;
                grid[object.y][object.x] = "S";
                grid[object.y - 1][object.x] = "E";
        }else if(grid[object.y + 1][object.x] == "S" || object.y==rows-1){
            object.x=coloumns/2;
            object.y=0;
        }
    }else if(object.y == rows-1){
            object.x=coloumns/2;
            object.y=0;
        } 
    drawObject();
    checkFullRow();
}
window.refresh = refresh;

function checkFullRow() {
    for (let row = 0; row < rows; row++) {
        let full = true;
        for (col = 0; col < coloumns; col++) {
            full = full && (grid[row][col] == "S");
        }
        if (full) {
            grid.splice(row, 1);
            grid.unshift(["E", "E", "E", "E", "E", "E", "E", "E", "E", "E"]);
            points++;
            pointsArea.innerHTML = "Points:"+points;
        }
    }
}

function move(event) {
    if(object.y<rows-1){
          if (event.key == 'a' && object.x > 0 && grid[object.y+1][object.x - 1] == "E") {
            object.x = object.x - 1;
            grid[object.y][object.x + 1] = "E";
        } else if (event.key == 'd' && object.x < 10 && grid[object.y+1][object.x + 1] == "E") {
            object.x = object.x + 1;
            grid[object.y][object.x - 1] = "E";
        } else if (event.key == 's' && object.y < (rows - 2) && grid[object.y + 1][object.x] == "E") {
            object.y = object.y + 1;
            grid[object.y - 1][object.x] = "E";
        }  
    }
    drawObject();
}
window.move= move;
drawGrid();
var updt = setInterval(refresh, 400);